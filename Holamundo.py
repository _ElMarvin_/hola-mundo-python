from math import *
from graphics import *
from time import *
Vo=60
t=0
W=37*3.1415926535/180
y=0
g=9.8

ventana=GraphWin("Simulador", 400, 400)
ventana.setCoords(0,0,400,400)



while (y>=0):
    x = Vo*cos(W)*t
    y = Vo*sin(W)*t-(1/2)*g*t*t

    miCirculo=Circle(Point(x,y),10)
    miCirculo.setFill('pink')
    miCirculo.draw(ventana)
    print(x,y)
    sleep(0.1)
    t = t+0.1

ventana.getMouse()